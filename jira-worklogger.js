// ==UserScript==
// @name         Time logger
// @namespace    http://tampermonkey.net/
// @version      0.2
// @description  Log your time on recently modified tickets
// @author       You
// @match        https://pindrop.atlassian.net/secure/Dashboard.jspa*
// @require      https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js
// @require      https://cdnjs.cloudflare.com/ajax/libs/jshashes/1.0.7/hashes.js
// @grant        GM.xmlHttpRequest
// ==/UserScript==

const jira_token = '' // get token at https://id.atlassian.com/manage/api-tokens
const jira_username = ''; //add jira account email
const domain = 'https://pindrop.atlassian.net';
const search_url = '/rest/api/3/search';
const worklog_url = '/rest/api/3/issue/{issueIdOrKey}/worklog';
const authData = '{jira_username}:{jira_token}';

function notify(title, message) {
    if (Notification.permission !== "granted")
        Notification.requestPermission();
    else {
        var notification = new Notification('Notification ', {
            title: title,
            body: message,
        });

        notification.onclick = function () {
            window.focus();
        };
        setTimeout(notification.close.bind(notification), 3000);
    }
}

function successfulSubmit(response) {
    if (response.status === 201) {
        notify('time submitted', 'work logged');
        $('.submit').val('');

    } else if (response.status >= 400) {
        notify('time submitted failed', response.responseText);
        console.log(response.responseText);
    }
    $('.submit').removeClass('submit')
}

function parseLogWorkRequest(time, text) {
    let date =  new Date().toISOString().replace('Z','+0000');
    let request = {
        "adjustEstimate" : 'auto',
        "timeSpent": time, "comment": {
            "type": "doc", "version": 1, "content": [{
                "type": "paragraph",
                "content": [
                    {
                        "text": text,
                        "type": "text"
                    }
                ]
            }
                                                    ]
        },
        "started": date
    };
    return JSON.stringify(request);
}

function log_work(obj) {
    if (jira_token === '') {
        alert('No JIRA token found. Get one at https://id.atlassian.com/manage/api-tokens');
    } else {

        let authString = authData.replace('{jira_username}', jira_username).replace('{jira_token}', jira_token);

        let SHA512 = new Hashes.SHA512;
        let key = SHA512.hex(authString);

        let ticketKey = obj.currentTarget.attributes[2].nodeValue;
        let time = $('.' + ticketKey + '.time').addClass('submit').val();
        let text = $('.' + ticketKey + '.description').addClass('submit').val();
        let url = domain + worklog_url.replace('{issueIdOrKey}', ticketKey);
        let atlassianToken = $('#atlassian-token')[0].content;
        let headers = {
            'Authorization': 'Basic ' + key,
            'Content-Type': 'application/json; charset=UTF-8',
            "Accept": "application/json",
            'atl_token': atlassianToken,
            "User-Agent":"xx",
            "X-Atlassian-Token":"nocheck",
        };
        let data = parseLogWorkRequest(time, text);
        GM.xmlHttpRequest({
            method: "POST",
            data: data,
            url: url,
            headers: headers,
            onload: function (response) {
                successfulSubmit(response);
            }
        });
    }

}

function createDataTable(response) {
    let jsonData = JSON.parse(response.responseText);
    let dataSet = jsonData.issues;
    $('#dataTable').DataTable({
        data: dataSet,
        aaSorting: [[0, 'desc']],
        columns: [
            {
                'data': 'fields.updated',
                "render": function (data, type, row, meta) {
                    return '<div style="width:50px;overflow:hidden">' + data + '</div>';
                }

            },
            {
                'data': 'key',
                "render": function (data, type, row, meta) {
                    return '<a href="https://pindrop.atlassian.net/browse/' + data + '" target="_blank">' + data + '</a>';
                }
            },
            {
                'data': 'fields.summary',
                "render": function (data, type, row, meta) {
                    return '<div style="width:200px;overflow:hidden">' + data + '</div>';
                }
            },
            {
                title: 'log work:',
                'data': 'id',
                "render": function (data, type, row, meta) {
                    return '<input class="' + data + ' time" style="width:75px;height:25px" target="_blank" />';
                }
            },
            {
                title: 'log description :',
                'data': 'id',
                "render": function (data, type, row, meta) {
                    return '<input class="' + data + ' description" style="height:25px" target="_blank" />';
                }
            },
            {

                'data': 'id',
                "render": function (data, type, row, meta) {
                    return '<button class="worklog_button" style="" data="' + data + '">log time</button>';
                },
                
            },

        ]
    });

    $('.worklog_button').on('click', function (obj) {
       log_work(obj)
    });
    $('#dataTable').DataTable().on('draw',function(){
        $('.worklog_button').off().on('click', function (obj) {
            log_work(obj)
        });
    });

}

function init_dashboard() {

    $('head').append('<link type="text/css" rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">');
    $('#gadget-10000-chrome').append('<div class="ac-content"><div class=" dashboard-item-content "><table id="dataTable" class="display aui  issue-table" width="100%"></table></div></div>');
    let url = domain + search_url + '?jql=(assignee%20%3D%20currentUser()%20or%20assignee%20was%20currentUser()%20or%20worklogAuthor%20%3D%20currentUser())%20AND%20status%20!%3D%20Closed%20ORDER%20BY%20updated';
    GM.xmlHttpRequest({
        method: "GET",
        url: url,
        onload: function (response) {
            createDataTable(response);
        }
    });

}

init_dashboard();


